import { Section, Heading } from 'react-bulma-components';
import { BsChatQuoteFill } from 'react-icons/bs';

function SiteHeading() {
  return (
    <Section>
      <Heading size={1}>
        Quotes <BsChatQuoteFill  size="1em" />
      </Heading>
      <Heading size={4} subtitle>
        Get, generate, vote.
      </Heading>
    </Section>
  );
}

export default SiteHeading;