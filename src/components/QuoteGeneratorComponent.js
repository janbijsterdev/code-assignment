import React, { useState } from 'react';
import { Form, Button, Heading } from 'react-bulma-components';
import ReactTagInput from "@pathofdev/react-tag-input";
import QuoteGenerator from '../utils/QuoteGenerator.js';
import { AiOutlineRobot } from 'react-icons/ai';

function QuoteGeneratorComponent({ setCurrentQuote }) {
  const [topics, setTopics] = useState(['internet', 'chorizo']);
  const [author, setAuthor] = useState('');

  async function generateQuote() {
    setCurrentQuote(await QuoteGenerator.generateQuote(author || 'anonymous', topics));
  }

  return (
    <React.Fragment>
      <Heading size="4">
        AI Quote generator
      </Heading>
      <Form.Field>
        <Form.Label>Author</Form.Label>
        <Form.Control>
          <Form.Input
            value={author}
            placeholder="Your name"
            onChange={(e) => setAuthor(e.target.value)}
          />
          <Form.Help>You push the button, you should get the credits</Form.Help>
        </Form.Control>
      </Form.Field>
      <Form.Field>
        <Form.Label>
          Topics
        </Form.Label>
        <Form.Control>
          <ReactTagInput 
            tags={topics} 
            onChange={(newTopics) => setTopics(newTopics)}
          />
          <Form.Help>What should the quote be about?</Form.Help>
        </Form.Control>
      </Form.Field>
      <Form.Field>
        <Button color="primary" onClick={generateQuote}>
          <AiOutlineRobot className="inline-icon" size="1.5em" />
          Generate quote
        </Button>
      </Form.Field>
    </React.Fragment>
  );
}

export default QuoteGeneratorComponent;