import { Card, Media, Heading, Content } from 'react-bulma-components';
import { BsChatQuoteFill } from 'react-icons/bs';

function QuoteCard({ quote, children, iconColor }) {
  return (
    <Card>
      <Card.Content>
        <Media>
          <Media.Item renderAs="figure" align="left" textColor={iconColor}>
            <BsChatQuoteFill  size="3em"/>
          </Media.Item>
          <Media.Item>
            <Heading size={4}>{ quote.author }</Heading>
            <Heading subtitle size={6}>
              { quote.origin }
            </Heading>
          </Media.Item>
        { quote.votes !== undefined && (
          <Media.Item renderAs="div" align="right">
            {quote.votes } Votes
          </Media.Item>
        )}
        </Media>
        <Content>
          { quote.quote }
        </Content>
        {children}
      </Card.Content>
    </Card>
  );
}

export default QuoteCard;