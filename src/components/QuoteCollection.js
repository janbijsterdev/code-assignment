import { Heading, Button, Section, Columns } from 'react-bulma-components';
import React, { useState, useEffect } from 'react';
import QuoteBackend from '../utils/QuoteBackend';
import QuoteCard from './QuoteCard';
import { AiOutlineSave, AiOutlineLike, AiOutlineDislike } from 'react-icons/ai';


function QuoteCollection({ currentQuote }) {
  let [quotes, setQuotes] = useState([]);

  async function getQuotes() {
    setQuotes(await QuoteBackend.read());
  }
  async function createQuote({ quote, author, origin }) {
    await QuoteBackend.create(quote, author, origin);
    getQuotes();
  }
  async function updateQuote(id, votes) {
    await QuoteBackend.update(id, votes);
    getQuotes();
  }

  // get quotes at render
  useEffect(() => {
    getQuotes();
  }, []);

  return (
    <React.Fragment>
      
        {currentQuote != null && (
          <QuoteCard quote={currentQuote} iconColor="warning">
            <Button 
              color="warning"
              onClick={() => createQuote(currentQuote)}
            >
              <AiOutlineSave className="inline-icon" size="1.5em" />
              Save current quote
            </Button>
          </QuoteCard>
        )}
        <Section>
          <Heading size="4">All quotes</Heading>
        </Section>
        <Columns>
          {quotes.map((quote, idx) => (
            <Columns.Column size={6} key={idx}>
            <QuoteCard quote={quote} >
              <Button.Group>
                <Button 
                  color="primary"
                  onClick={() => updateQuote(quote.id, quote.votes + 1)}
                >
                  <AiOutlineLike className="inline-icon" size="1.5em" />
                  Upvote
                </Button>
                <Button 
                  color="warning"
                  onClick={() => updateQuote(quote.id, quote.votes - 1)}
                >
                  <AiOutlineDislike className="inline-icon" size="1.5em" />
                  Downvote
                </Button>
              </Button.Group>
            </QuoteCard>
            </Columns.Column>
          ))}
        </Columns>
    </React.Fragment>
  );
}

export default QuoteCollection;