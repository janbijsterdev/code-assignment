import { Heading, Button } from 'react-bulma-components';
import React from 'react';
import QuoteApi from '../utils/QuoteApi';
import { AiOutlineCloudDownload } from 'react-icons/ai';


function QuoteApiComponent({ setCurrentQuote }) {

  async function getQuote() {
    setCurrentQuote(await QuoteApi.getRandomQuote());
  }

  return (
    <React.Fragment>
      <Heading size="4">API</Heading>
        <Heading size="6" subtitle>
          Get a random quote from the Storm Consultancy Programming Quotes API
          </Heading>
        <Button fullwidth color="info" onClick={getQuote} className="button-bottom">
          <AiOutlineCloudDownload className="inline-icon" size="1.5em" />
          Get new random quote
        </Button>
    </React.Fragment>
  );
}

export default QuoteApiComponent;