import SiteHeading from './components/SiteHeading';
import QuoteCollection from './components/QuoteCollection';
import QuoteGeneratorComponent from './components/QuoteGeneratorComponent';
import QuoteApiComponent from './components/QuoteApiComponent';
import { useState } from 'react';
import { Container, Section, Box, Tile } from 'react-bulma-components';
import "@pathofdev/react-tag-input/build/index.css";
import './App.css';

function App() {
  const [currentQuote, setCurrentQuote] = useState(null);

  return (
    <div className="App">
      <Container>
        <SiteHeading />
        <Section>
          <Tile kind="ancestor">
            <Tile kind="parent">
              <Tile renderAs={Box}  kind="child" >
                <QuoteApiComponent setCurrentQuote={setCurrentQuote} />
              </Tile>
            </Tile>
            <Tile size={8} kind="parent">
              <Tile renderAs={Box} kind="child" color="primary">
                <QuoteGeneratorComponent setCurrentQuote={setCurrentQuote} />
              </Tile>
            </Tile>
          </Tile>
          <QuoteCollection currentQuote={currentQuote} />
        </Section>
      </Container>
    </div>
  );
}

export default App;
