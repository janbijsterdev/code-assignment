const QuoteGenerator = {
  apiUrl:  './backend/generate.php', // 'http://localhost:8888/quotes/generate.php',
  stripText(s) {
    // Cutoff remainder after the last '.', '?' or '!', if found.
    // Based on https://stackoverflow.com/a/53901610/8811010
    const matches = s.match(/.*?[?!.]/g);
    return matches ? matches.join(' ') : s;
  },
  async generateQuote(author, topics) {
    const formData = new FormData();
    topics.forEach(topic => {
      formData.append('topics[]', topic);
    });
    
    const response = await fetch(this.apiUrl, {
      method: 'POST',
      body: formData
    })
    const result = await response.json();
    if (result.success) {
      const quote = this.stripText(result.data);
      return { author, quote, origin: 'generated' }
    } else {
      console.warn(result.message);
    }
  },
};

export default QuoteGenerator;