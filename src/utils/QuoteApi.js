const QuoteApi = {
  quotesApiBaseUrl: 'http://quotes.stormconsultancy.co.uk',
  async getRandomQuote() {
    const result = await fetch(`${this.quotesApiBaseUrl}/random.json`);
    const quote = await result.json();
    return { ...quote, origin: 'api' };
  }
};

export default QuoteApi;