const QuoteBackend = {
  apiBaseUrl:  './backend', // 'http://localhost:8888/quotes',
  async read() {
    const response = await fetch(`${this.apiBaseUrl}/read.php`);
    const result = await response.json();
    if (result.data) {
      // sort quotes by votes descending
      return result.data.sort((a, b) => b.votes - a.votes);
    } else {
      return []
    }
  },
  async create(quote, author, origin) {
    const formData = new FormData();
    formData.append('quote', quote);
    formData.append('author', author);
    formData.append('origin', origin);
    const response = await fetch(`${this.apiBaseUrl}/create.php`, {
      method: 'POST',
      body: formData
    })
    const result = await response.json();
    if (result.success) {
      return result.message;
    } else {
      console.warn(result.message);
    }
  },
  async update(id, votes) {
    const formData = new FormData();
    formData.append('id', id);
    formData.append('votes', votes);
    const response = await fetch(`${this.apiBaseUrl}/update.php`, {
      method: 'POST',
      body: formData
    })
    const result = await response.json();
    if (result.success) {
      return result.message;
    } else {
      console.warn(result.message);
    }
  }
};

export default QuoteBackend;