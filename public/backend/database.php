<?php

// don't allow direct request
defined('DIRECT_REQUEST_ALLOWED') || die("No direct access to this script.");

class Database {
  
    private $dbPath = "quotes.db";

    private $db;
  
    // get the database connection
    function __construct() {
        $this->db = new SQLite3($this->dbPath);
    }

    public function read() {
        // select all query
        // no user-provided parameters, simple query will do
        $results = $this->db->query('SELECT * FROM quotes');
        $quotes = [];
        while ($quote = $results->fetchArray(SQLITE3_ASSOC)) {
            $quotes[] = $quote;
        }
        return $quotes;
    }

    public function create($quote, $author, $origin) {
        $statement = $this->db->prepare('INSERT INTO quotes (quote, author, origin) VALUES (:quote, :author, :origin);');
        $statement->bindValue(':quote', $quote);
        $statement->bindValue(':author', $author);
        $statement->bindValue(':origin', $origin);
        $statement->execute();
    }

    public function update($id, $votes) {
        $statement = $this->db->prepare('UPDATE quotes SET votes = :votes WHERE id = :id;');
        $statement->bindValue(':id', $id);
        $statement->bindValue(':votes', $votes);
        $statement->execute();
    }
}

?>