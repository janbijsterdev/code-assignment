<?php

// allow direct request
define("DIRECT_REQUEST_ALLOWED", true);

include_once "database.php";
include_once "common.php";


if ($_SERVER["REQUEST_METHOD"] !== "GET") {
    return_output(false, "Wrong method.");
}

// instantiate database 
$db = new Database();
// get quotes
$quotes = $db->read();
$count = count($quotes);
return_output(true, "$count quote(s) read.", $quotes);