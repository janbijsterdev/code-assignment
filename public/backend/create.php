<?php

// allow direct request
define("DIRECT_REQUEST_ALLOWED", true);

include_once "database.php";
include_once "common.php";


if ($_SERVER["REQUEST_METHOD"] !== "POST") {
    return_output(false, "Wrong method.");
}

// validation
// is a quote given?
if (isset($_POST["quote"])) {
    $quote = $_POST["quote"];
} else {
    return_output(false, "No quote passed.");
}

if (isset($_POST["author"])) {
    $author = $_POST["author"];
} else {
    $author = "anonymous";
}

if (isset($_POST["origin"])) {
    $origin = $_POST["origin"];
} else {
    $origin = "unknown";
}

// instantiate database 
$db = new Database();
// create
$db->create($quote, $author, $origin);

return_output(true, "Quote created.");