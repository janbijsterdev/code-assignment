<?php

// Endpoint for querying the ml6team gpt-2-medium-conditional-quote-generator
// (see https://huggingface.co/ml6team/gpt-2-medium-conditional-quote-generator)
// Using Huggingface"s Hosted Accelerated Inference API

// allow direct request
define("DIRECT_REQUEST_ALLOWED", true);

// incrrease timeout limit for waking up the model:
set_time_limit(120);

include_once "common.php";

if ($_SERVER["REQUEST_METHOD"] !== "POST") {
    return_output(false, "Wrong method.");
}

// validation
// are topics given?
if (isset($_POST["topics"])) {
    $topics = $_POST["topics"];
} else {
    return_output(false, "No topics passed.");
}

// from prompt
$prompt = "Given Topics: " . implode(" | ", $topics) . ". Related Quote:";
// set options
$payload = json_encode([
  "inputs" => $prompt,
  "options" => [
    "wait_for_model" => true
  ]
]);


// API URL
$url = "https://api-inference.huggingface.co/models/ml6team/gpt-2-medium-conditional-quote-generator";

// Create a new cURL resource
$ch = curl_init($url);

// Attach encoded JSON string to the POST fields
curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

// Set the content type to application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    "Content-Type:application/json",
    'Content-Length: ' . strlen($payload),
    "Authorization:Bearer api_tubrLCkhhJYdOejodYEEAUpvAeYWYZofXf"
));

// Return response instead of outputting
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

// Execute the POST request
$result = curl_exec($ch);

// Close cURL resource
curl_close($ch);

try {
    $generatedText = json_decode($result, true)[0]["generated_text"];
    $quote = substr($generatedText, strlen($prompt));
    return_output(true, "Quote generated with prompt [$prompt]", $quote);
} catch (Exception $e) {
    return_output(false, "Problem decoding output: $result");
}

