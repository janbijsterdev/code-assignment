<?php

// don't allow direct request
defined('DIRECT_REQUEST_ALLOWED') || die("No direct access to this script.");


function return_output($success = false, $message = "", $data = []) {
    // required headers
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");

    echo json_encode(["success" => $success, "message" => $message, "data" => $data]);
    exit();
}