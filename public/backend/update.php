<?php

// allow direct request
define("DIRECT_REQUEST_ALLOWED", true);

include_once "database.php";
include_once "common.php";


if ($_SERVER["REQUEST_METHOD"] !== "POST") {
    return_output(false, "Wrong method.");
}

// validation
// is an id given?
if (isset($_POST["id"])) {
    $id = $_POST["id"];
} else {
    return_output(false, "No id passed.");
}

// is a number of votes given?
if (isset($_POST["votes"])) {
    $votes = $_POST["votes"];
} else {
    return_output(false, "No votes passed.");
}

// instantiate database 
$db = new Database();
// update
$db->update($id, $votes);

return_output(true, "Quote updated.");