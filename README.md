# Kabisa Code Assignment: Quotes webapp

## Demo

See the endresult at http://janbijster.com/show/kabisa/

## Assignment
Web app showing random quotes

### Possible functionalites

- Fetch quotes from an external service (eg: http://quotes.stormconsultancy.co.uk/api )
- Share ‘quotes’ on social media, like Facebook or Twitter
- Allow users to rate quotes and view other people their ratings
- Tweak the random quote logic to prioritize showing highly rated quotes to new users
- Find quotes comparable to the currently shown quote
- Create a slideshow of random quotes being shown
- Create a mobile friendly application, keeping different devices in mind

### Resources

- 3-5 hours
- make use of libraries / api's / external reources as needed

___

## Plan

### Goal

Show off:
- coding skills (write nice code)
- experience (make wise, pragmatic choices)
- creatvity (deliver unexpected results)

### Chosen functionalities:
- Load quotes from api
- rate / like quotes
- likes influences probabilities of quotes shown
- responsive / mobile friendly (offcourse)

Try an additional functionality:
- Generate quote with starting prompt / topic using GPT-2 or Markov model.

### Implementation backend
Recording likes requires a backend. Options:
- Simple PHP script
- Backend framework + api (e.g. Laravel)
- Backend as a service (e.g. firebase)

Considerations:

- Don't spend too much time on the backend, setting up a Laravel project could easily consume all the hours.
- Using firebase with authentication is involved, can take long. Without authentication unsecure.
- Not many simultaneous requests needed, sqlite will do and is easy to deploy

-> choice: use simple php scripts for the backend and an sqlite db

### Implementation frontend

Use js framework in combination with (mobile-friendly) frontend  CSS library (https://geekflare.com/best-css-frameworks/)

choice: vue.js experience clear from CV, use React for this one. Use Bulma: good looking, well documented


### Implementation generating quotes

Find pretrained model on Huggingface hosted inference api, use that api.
Possibly needed to route that api call through the ackend to prevent cors problems.

___

## Hours

- Plan: 0.5 hr
- Setup project (react-bulma): 0.5hr
- Storm quotes api: 1.5 hr
- Generate quote: 2 hr
- php backend: 1 hr
- quote voting: 2 hr
- final design tweaks 1 hr
- code tidying & refactoring 0.5 hr

total 8 hr


## Develpment notes

### Available Scripts

In the project directory, you can run:

#### `yarn start` / `npm run start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

#### `yarn build` / `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

#### `yarn eject` / `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.
